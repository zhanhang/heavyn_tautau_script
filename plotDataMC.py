#lsetup "root 6.30.02-x86_64-centos7-gcc11-opt"

import os
from collections import defaultdict
import ROOT
import time 
import sys 
import math

ROOT.gROOT.SetStyle("ATLAS")
ROOT.gROOT.ForceStyle()
ROOT.gROOT.SetBatch(ROOT.kTRUE)
ROOT.ROOT.EnableImplicitMT()

#use this for v0 plot
#inputdir="/eos/atlas/atlascerngroupdisk/phys-exotics/lpx/ANA-EXOT-2022-34_TauPlusX/TauX_v0/nominal/"
#use this for v1 plot
inputdir="/eos/atlas/atlascerngroupdisk/phys-exotics/lpx/ANA-EXOT-2022-34_TauPlusX/TauX_v1/nominal/"

#inputdir_dijet_ttbar="/afs/cern.ch/work/y/yelghaza/public/HNL/ttbarallhadd_dijets"




def sampleDISD():
    # taken from https://docs.google.com/spreadsheets/d/1Kud23UvpA00kiKYl20EFNjKK00zpe--ZipOxpTNSqbs/edit#gid=1309254385
    dsids = {
        "Top":[[346345, 346344, 407344, 407343, 407342, 410472, 410470], ROOT.kViolet],
        "Vgamma":[[700404, 700403, 700401, 700400, 700399, 700398, 700402], ROOT.kViolet+10],
        "VVV":[[364249, 364248, 364247, 364246,364245, 364244, 364243, 364242], ROOT.kRed-3],
        "Diboson":[[363489, 363360, 363358, 363357, 363356, 363355, 345723, 345718], ROOT.kAzure+9],
        "ggllll":[[345706,345705,364290,364289,364288,364287,364286,364285, 364284,364283,364255,364254,364253,364250], ROOT.kViolet+7],
        "Zjets":[[700337, 700336, 700335, 700334, 700333, 700332, 700331, 700330, 700329, 700328, 700327, 700326, 700325, 700324, 700323, 700322,700321, 700320], ROOT.kGreen-1],
        "Wjets":[[700349,700348, 700347,700346,700345,700344, 700343, 700342, 700341,700340, 700339, 700338], ROOT.kBlue+3],
        "ttV":[[700000, 410155, 504554,  500800, 410278, 410277, 410276, 410220, 410219, 410218, 410157, 410156, 410408, 410560,410647, 410646], ROOT.kCyan],
        "stop":[[410645, 410644, 410659, 410658], ROOT.kMagenta-6],
        "FourTops":[[412044, 412043, 410081, 304014], ROOT.kRed-2],
        #"ttbar":[[410471], ROOT.kViolet-3],
        #"Dijet":[[364700,364701,364702,364703,364704,364704,364705,364706,364707,364708,364709,364010,364711,364712], ROOT.kBlue-3],
    }

    return dsids



def getListOfFiles():
    listfiles = {
                "mc20a": defaultdict(list),
                "mc20d": defaultdict(list),
                "mc20e": defaultdict(list),
                }

    for dirpath, dirnames, filenames in os.walk(inputdir):

        if not "data" in dirpath and not "p5855" in dirpath: continue
     
        for filename in filenames:
            filepath = os.path.join(dirpath, filename)

            if "data" in filename:
                if "data15" in filename or "data16" in filename:
                    listfiles["mc20a"]["data"].append(filepath)
                if "data17" in filename:
                    listfiles["mc20d"]["data"].append(filepath)
                if "data18" in filename:
                    listfiles["mc20e"]["data"].append(filepath)
                #print("\t Found {}".format(filename))
            
            for samplename, list_dsids in sampleDISD().items():
                for dsid in list_dsids[0]:
                    filedisd=""
                    if "AF" in filename: filedisd = filename[:filename.index("_")]
                    if not "AF" in filename: filedisd = filename[:filename.index(".root")]
                    #print("filedsid", filedisd)
                    if str(dsid) == filedisd:
                        
                        if "mc20a" in filepath: listfiles["mc20a"][samplename].append(filepath)
                        elif "mc20d" in filepath: listfiles["mc20d"][samplename].append(filepath)
                        elif "mc20e" in filepath: listfiles["mc20e"][samplename].append(filepath)
                        else:
                            Print("file {} doesnt match to any mc compaign".format(filepath))

        
    
    return listfiles


def getRelErrors(hist):
    h_err = hist.Clone()

    h_err.Reset()

    for bin in range(0, hist.GetNbinsX()+1):
        binContent = hist.GetBinContent(bin)
        error = hist.GetBinError(bin)
        if binContent != 0:
            h_err.SetBinContent(bin, 0)
            h_err.SetBinError(bin, error / binContent)
            print("error {}".format(error/binContent))
        h_err.SetDirectory(0)
    return h_err


# Data/MC ratio 
def getDataMCComparaison(h_data, h_bkg):
  h_comp = h_data.Clone()
  h_comp.Reset()

  for bin in range(0, h_comp.GetNbinsX()):
    n_data = h_data.GetBinContent(bin)
    e_data = h_data.GetBinError(bin)
    n_bkg = h_bkg.GetBinContent(bin)

    if(n_data > 1e-6 and e_data > 1e-6 and n_bkg > 1e-6):
      n_comp = n_data  / n_bkg
      e_comp = e_data / n_bkg
      h_comp.SetBinContent(bin, n_comp)
      h_comp.SetBinError(bin, e_comp)

  return h_comp



def get_event_weight(mc_period, isdata):
    print("setting event weight for mc_period: {}".format(mc_period))
    if isdata:
        return "trigger_decision"  
    else:     
        if mc_period=="mc20a":
             return "36207.66*weight_pileup*weight_mc*xs/totalEventsWeighted"                    
        if mc_period=="mc20d":
             return "44307.4*weight_pileup*weight_mc*xs/totalEventsWeighted"             
        if mc_period=="mc20e":
             return "58450.1*weight_pileup*weight_mc*xs/totalEventsWeighted" 
         



def gethist(r_files, samplename, mc_compaing, cuts, triggers,variablename, Nbins, xlow, xhigh):
    '''Get Hist for a set of cuts'''
    hist_sum = ROOT.TH1D()
    vecList =  ROOT.std.vector('string')()
    for element in r_files: vecList.push_back(element)
    df = ROOT.RDataFrame("nominal", vecList)
    df =df.Define("detajj_01","float d=abs(jet_Eta_0-jet_Eta_1);return d;").Define("taus_charge_01","taus_charge_0*taus_charge_1").Define("dphi_taunu_0","float dp0=abs(tau_Phi_0-met_phi);return dp0").Define("dphi_taunu_1","float dp1=abs(tau_Phi_1-met_phi);return dp1")

    
    if triggers!="none":
       df=df.Filter(triggers)

    if not "data" in samplename: 
        event_weight = get_event_weight(mc_compaing,False)
        dfilter = df.Filter(cuts).Define("weight_new", event_weight)
        hist = dfilter.Define("rescale_variable",variablename).Histo1D(("", "", Nbins, xlow,xhigh), "rescale_variable", "weight_new")
        hist_sum= hist.Clone()


    else:
        event_weight = get_event_weight(mc_compaing,True)
        dfilter = df.Filter(cuts)
        hist=dfilter.Define("rescale_variable",variablename).Histo1D(("","",Nbins,xlow,xhigh),"rescale_variable")
        hist_sum= hist.Clone()

    hist_sum.SetDirectory(0) 
    histname = samplename +"_"+mc_compaing
    hist_sum.SetName(histname)

    print("\t Found hist named {} Entries {}".format(histname,hist_sum.Integral() ))
    return hist_sum


def merg_hists(h_name, hists):
    h_sum = ROOT.TH1D()
    for h in hists:
        if h_sum.Integral()==0.:
            h_sum = h.Clone()
        else:
            h_sum.Add(h)
    h_sum.SetDirectory(0)
    h_sum.SetName(h_name)
    return h_sum

def get_merged_hists(tmp_hists):
    '''
    histograms are made per mc periods (cause needed a careful weight for each of them)
    this function will merge hists of the same mc campaign 
    '''
   
    print("merging hists ..........")
    dic_hists = defaultdict(list)
    for tmp_h in tmp_hists:
        h_name = tmp_h.GetName()[:tmp_h.GetName().index("mc20")-1]
        print(h_name)
        dic_hists[h_name].append(tmp_h)
    
    merged_hists =  list()
    for s, hs in dic_hists.items():
        h = merg_hists(s, hs)
        merged_hists.append(h)
       
    for h in merged_hists:
        print("{} {}".format(h.GetName(), h.Integral()))
    return merged_hists





def getHistograms(cuts,triggers,cutsLabel, variablename,variableLabel, Nbins, xlow, xhigh, weight):
    listoffiles = getListOfFiles()
    tmp_hists = list()

    for mc_compaing, samples in listoffiles.items():
        for samplename, samplefiles in samples.items():
            hist = gethist(samplefiles, samplename, mc_compaing, cuts, triggers,variablename, Nbins, xlow, xhigh)
            tmp_hists.append(hist)
    for tmp_h in tmp_hists:
        print(tmp_h.GetName(), tmp_h.Integral())

    hists = get_merged_hists(tmp_hists)
    
    

    for h in hists:
        print("final",h.GetName(), h.Integral())


    c1 = ROOT.TCanvas('C1', 'Data MC', 200, 10, 800, 800)
    padUp = ROOT.TPad('pad_u', 'pad up',0, 0.3, 1, 1)
    padDown = ROOT.TPad('pad_d', 'pad down',0, 0, 1, 0.3)
    
    padDown.SetBottomMargin(0.3)
    padDown.SetTopMargin(0)
    padDown.SetRightMargin(0.08)
    padUp.SetTopMargin(0.05)
    padUp.SetBottomMargin(0.02)
    padUp.SetRightMargin(0.08)

    padUp.Draw()
    padDown.Draw()
    padUp.cd()
    leg = ROOT.TLegend(0.95,0.63,0.7,0.92)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg.SetFillStyle(0)
    leg.SetLineStyle(1)
    leg.SetTextSize(0.03)
    hist_data = ROOT.TH1D()
    hist_bkg = ROOT.TH1D()

    #get data and total bkg hist
    for hist in hists:
        if hist.GetName()=="data": hist_data =  hist.Clone()
        else:
            if (hist_bkg.Integral()==0.): hist_bkg =hist.Clone()
            else: hist_bkg.Add(hist)

    hists.sort(key=lambda x: x.Integral()) # sorted list for low-entry hist -> high-entry
    hstack =  ROOT.THStack()
    for hist in hists:
        if "data" in hist.GetName(): continue
        hist.SetFillColor(sampleDISD()[hist.GetName()][1])
        hist.SetMarkerColor(sampleDISD()[hist.GetName()][1])
        hist.SetLineColor(sampleDISD()[hist.GetName()][1])
        hstack.Add(hist)

    for h in reversed(hists): 
        if h.GetName()=="data": continue
        leg.AddEntry(h, h.GetName(),"F")
    hist_bkg.SetDirectory(0)
    hist_data.SetDirectory(0)

    hstack.Draw("hist")
    hstack.GetHistogram().GetXaxis().SetLabelOffset(999)
    hstack.GetHistogram().GetYaxis().SetTitle("Events")
    hstack.GetHistogram().GetYaxis().SetTitleSize(0.13)
    hstack.SetMinimum(10)

    hist_data.SetMarkerStyle(20)
    hist_data.SetLineWidth(2)
    hist_data.SetFillColor(ROOT.kBlack)
    hist_data.SetMarkerColor(ROOT.kBlack)
    hist_data.SetLineColor(ROOT.kBlack)
    hist_data.Draw("epsames")
    leg.AddEntry(hist_data, "Data", "lep")

    #stat uncertainties 
    #h_uncert = getRelErrors(hist_bkg) 
    #leg.AddEntry(h_uncert, "Stat", "f")
      #For now, let's do not draw error
    leg.Draw('same')


    #paddown 
    padDown.cd()
    h_ratio=ROOT.TH1D()

    h_ratio = getDataMCComparaison(hist_data, hist_bkg) 

    h_ratio.GetYaxis().SetTitle('Data/Bkg')
    h_ratio.GetXaxis().SetTitle(variableLabel)
    h_ratio.GetXaxis().SetTitleSize(0.13)
    h_ratio.GetXaxis().SetTitleOffset(0.95)
    h_ratio.GetXaxis().SetLabelSize(0.1)
    h_ratio.GetYaxis().SetTitleSize(0.12)
    h_ratio.GetYaxis().SetTitleOffset(0.47)
    h_ratio.GetYaxis().SetLabelSize(0.1)
    h_ratio.GetYaxis().SetRangeUser(0, 2)
    h_ratio.Draw("ep")

    #h_uncert.SetMarkerSize(0)
    #h_uncert.SetFillColor(ROOT.kOrange + 6)
    #h_uncert.SetLineColor(1)
    #h_uncert.SetFillStyle(3001)
    #h_uncert.Draw("e3sames")

    padUp.cd()
    tex1 =ROOT.TLatex(0.2, 0.88, "ATLAS")
    tex1.SetNDC()
    tex1.SetTextFont(72)
    tex1.SetTextSize(0.05)
    tex1.SetTextSize(0.06)
    tex1.SetLineWidth(2)
    tex1.Draw()

    tex2 = ROOT.TLatex(0.36,0.88,"Internal")
    tex2.SetNDC()
    tex2.SetTextFont(42)
    tex2.SetTextSize(0.05)
    tex2.SetLineWidth(2)
    tex2.Draw()

    tex3 = ROOT.TLatex(0.20,0.84, "#sqrt{s}=13 TeV," +luminosity +"fb^{-1}")
    tex3.SetNDC()
    tex3.SetTextFont(42)
    tex3.SetTextSize(0.04)
    tex3.SetLineWidth(2)
    tex3.Draw("sames")

    tex4 = ROOT.TLatex(0.20,0.80, cutsLabel.replace("_", " "))
    tex4.SetNDC()
    tex4.SetTextFont(42)
    tex4.SetTextSize(0.04)
    tex4.SetLineWidth(2)
    tex4.Draw("sames")


    hstack.SetMaximum(hist_data.GetMaximum()*10)

    ROOT.gPad.SetLogy()
    outputname = cutsLabel.replace(" ", "_") + "_" + variablename.replace("*0.001","")
    c1.Print(outputname+'_Log_DTT_STT.png')


if __name__=="__main__":
    startTime=time.time()


    cuts = "total_leptons == 0 && nJets_OR>=2 && nTaus_OR>=2  && taus_JetRNNSigTight_0==1 && taus_JetRNNSigTight_1==1 && taus_charge_0*taus_charge_1<0"
    
    #triggers="none"
    triggers="flag_DTT_decision_matched || flag_STT_decision_matched "

    getHistograms(cuts, triggers,"OS CR", "tau_Pt_0*0.001", "tau_Pt_0/GeV",20, 0, 600, "totalEventsWeighted")
       # use variable*0.001 to convert the unit  from MeV to GeV
    #getHistograms(cuts, triggers,"OS CR", "jet_Eta_0", "jet_Eta_0",20, -5, 5, "totalEventsWeighted")
    

    endTime=time.time()
    print("Took {} min to collect histogram and make workspace".format((endTime-startTime)/60))

